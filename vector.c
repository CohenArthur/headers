#include <stdlib.h>
#include <err.h>

#include "vector.h"

struct vector *vector_init(size_t capacity, void (*free_fnct)(void*)) {
    if (capacity == 0)
        errx(1, "Can't create a Vector with capacity of zero.");
    struct vector *new_v = malloc(sizeof(struct vector));
    void *new_data = malloc(sizeof(void*) * capacity);
    if (!new_v || !new_data)
        return NULL;

    new_v->data = new_data;
    new_v->capacity = capacity;
    new_v->index = 0;
    new_v->free_fnct = free_fnct;
   
   return new_v;
}

void vector_free(struct vector *v) {
    if (v->free_fnct)
        for (size_t index = 0; index < v->index; index++)
            v->free_fnct(v->data[index]);

    free(v->data);
    free(v);
}

static int vector_realloc(struct vector *vector) {
	vector->capacity = vector->capacity * 2;
	
	vector->data = realloc(vector->data, sizeof(void*) * vector->capacity);
	if (!vector->data)
		return 0;
	return 1;
}

int vector_push(struct vector *v, void *item){
	++v->index;
   	if (v->index == v->capacity)
   		if (vector_realloc(v) == 0)
			return 0;
    
	v->data[v->index] = item;

    return 1;
}

void *vector_pop(struct vector *v) {
    void *value = v->data[v->index];
	v->index--;
    return value;
}

void vector_reset(struct vector *v) {
    for (size_t index = 0; index < v->index; index++) {
        if (v->free_fnct)
            v->free_fnct(v->data[index]);
        v->data[index] = NULL;
    }

    v->index = 0;
}

static void vector_swap(void *left, void *right) {
	void *temp = left;
	left = right;
	right = temp;
} 

static int partition(struct vector *v, int min, int max) {
	void *pivot = vector_at(v, max);
	int index = min - 1;

	for(int index_pivot = min; index_pivot < max; ++index_pivot)
		if (v->compare(vector_at(v, index_pivot), pivot) == -1) {
			++index;
			vector_swap(vector_at(v, index), vector_at(v, index_pivot));
		}

	vector_swap(vector_at(v, index + 1), vector_at(v, max));

	return index + 1;
}

static void quicksort(struct vector *v, int min, int max) {
	if (min < max) {
		int pivot = partition(v, min, max);

		quicksort(v, min, pivot - 1);
		quicksort(v, pivot + 1, max);
	}
}

//FIXME
void vector_sort_ab(struct vector *vec, int min, int max) {
	if (!vec->compare)
		errx(1, "Vector has no compare function defined");
	quicksort(vec, min, max);
}

//FIXME
void vector_sort(struct vector *v) {
	if (!v->compare)
		errx(1, "Vector has no compare function defined");
	quicksort(v, 1, (int) v->index);
}
