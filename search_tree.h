#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define ST_ERROR 	0
#define ST_INSERTED 1
#define ST_REPLACED 2

typedef struct search_tree {
	char *link;
	long hash;
	
	struct search_tree *left;
	struct search_tree *right;
} search_tree;

/**
 * One At A Time algorithm
 */
long hash(char *key);

/**
 * Initializes a search_tree with an empty root node. Free it with search_tree_destroy()
 */
search_tree *search_tree_init();

/**
 * Frees the memory allocated to the tree and all the children of the root node.
 */
void search_tree_destroy(search_tree *tree);

/**
 * Inserts a new link in the search tree, at the correct position
 */
int search_tree_insert(search_tree *tree, char *link);
