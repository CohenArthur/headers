#include "shmap.h"

struct shmap *sh_init(size_t capacity) {
	if (capacity <= 0)
		errx(1, "Cannot init hmap with capacity lower or equal to 0");	

	struct shmap *new_map; 
	if (!(new_map = malloc(sizeof(struct shmap))))
		errx(1, "Creating the hmap failed");

	new_map->capacity = capacity;
	new_map->size = 0;

	struct shnode **new_array; 
	if (!(new_array = malloc(sizeof(struct shnode) * capacity)))
		errx(1, "Creating the hnode array failed");

	new_map->array = new_array;
	*(new_map->array) = NULL;
	
	return new_map;
}

void sh_free(struct shmap *map) {
	for(size_t node_i = 0; node_i < map->size; ++node_i)
		free(map->array[node_i]);

	free(map->array);
	free(map);
}

static void sh_realloc(struct shmap *map) {
	map->capacity = map->capacity * 2;
	map->array = realloc(map->array, map->capacity * sizeof(struct shnode));
}

void sh_insert(struct shmap *map, char *value) {
	if (map->size + 1 == map->capacity)
			sh_realloc(map);

	struct shnode *new_node = malloc(sizeof(struct shnode));
	new_node->next = NULL;
	new_node->hash = str_hash(value);
	new_node->value = value;

	if (!map->size)
		*(map->array) = new_node;
	else {
		struct shnode *current = *(map->array);
		while(current->next)
			current = current->next;

		current->next = new_node;
		map->array[map->size] = new_node;
	}
	++map->size;
}

struct shnode *sh_get_node(struct shmap *map, long key) {
	struct shnode *current = *(map->array);
	if (map->size)
		while(current) {
			if (current->hash == key)
				return current;
			current = current->next;
		}
	
	return NULL;
}

char *sh_get(struct shmap *map, long key) {
	struct shnode *node_value = sh_get_node(map, key);
	if (node_value)
		return node_value->value;
	return NULL;
}
