CC=gcc
CFLAGS= -Wall -Wextra -Werror -g

SRC= unit_tests.c linked_list.c test.c tdd.c vector.c hashmap.c \
	 libstr.c simple_vector.c shmap.c search_tree.c heap.c
OBJ=${SRC:*.c=*.o}

all : unit_tests

unit_tests : ${OBJ}

clean :
	${RM} *.o *.d main test
