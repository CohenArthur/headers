#include "hashmap.h"

static void *xmalloc(size_t size) {
	void *result = malloc(size);

	if(!result)
		errx(1, "Malloc failed.");

	return result;
}

static int xstrcmp(void *left, void *right) {
	return strcmp((char *) left, (char *) right);
}

static int xstrcasecmp(void *left, void *right) {
	return strcasecmp((char *) left, (char *) right);
}

struct hashmap *hm_create(size_t capacity, hashfunction hash, comparefunction compare, int flags) {
	struct hashmap *new_hash = malloc(sizeof(struct hashmap));
	if(!new_hash)
		return NULL;

	new_hash->flags = flags;
	new_hash->capacity = capacity;
	new_hash->size = 0;
	new_hash->hash = hash;
	new_hash->compare = compare;
	new_hash->fvalue = NULL;
	new_hash->fkey = NULL;

	struct hm_node **array = xmalloc(sizeof(struct hm_node) * capacity);

	new_hash->array = array;
	*(new_hash->array) = NULL;
	
	return new_hash;
}

struct hashmap *hm_create_str(size_t capacity, int flags) {
	struct hashmap *new_hash = malloc(sizeof(struct hashmap));
	if(!new_hash)
		return NULL;

	new_hash->flags = flags;
	new_hash->capacity = capacity;
	new_hash->size = 0;
	new_hash->hash = NULL; //Use default hash functions for strings ?
	new_hash->compare = xstrcmp;
	new_hash->fvalue = free;
	new_hash->fkey = free;

	struct hm_node **array = xmalloc(sizeof(struct hm_node) * capacity);

	new_hash->array = array;
	
	return new_hash;
}

struct hashmap *hm_create_str_ignore_case(size_t capacity, int flags) {
	struct hashmap *new_hash = malloc(sizeof(struct hashmap));
	if(!new_hash)
		return NULL;

	new_hash->flags = flags;
	new_hash->capacity = capacity;
	new_hash->size = 0;
	new_hash->hash = NULL; //Use default hash functions for strings ?
	new_hash->compare = xstrcasecmp;
	new_hash->fvalue = free; 
	new_hash->fkey = free; 	 

	struct hm_node **array = xmalloc(sizeof(struct hm_node) * capacity);

	new_hash->array = array;
	
	return new_hash;
}

//FIXME
void hm_destroy(struct hashmap *map) {
	for(size_t node_i = 0; node_i < map->size; ++node_i)
		hm_node_destroy(map, map->array[node_i]);
	free(map->array);
	free(map);
}

void hm_node_destroy(struct hashmap *map, struct hm_node *node) {
	if(map->fvalue)
		map->fvalue(node->value);
	if(map->fkey)
		map->fkey(node->key);
	free(node);
}

void hm_clear(struct hashmap *map) {
	for(size_t index = 0; index < map->size; index++) {
		if(map->fvalue)
			map->fvalue(map->array + index);
		*(map->array + index) = NULL;
	}
}

struct hm_node *hm_get_node(struct hashmap *map, void *key) {
	struct hm_node *node = *(map->array);

	if (map->size)
		while(node) {
		// hash(key) == node->hash ?
			if(map->compare(node->key, key) == 0) // (!map->compare(..)) ?
				return (node);
			node = node->next;
		}

	return NULL;
}

void *hm_get(struct hashmap *map, void *key) {
	struct hm_node *node = hm_get_node(map, key);
	if (node)
		return node->value; 
	return NULL;
}

static void hm_realloc(struct hashmap *map) {
	map->capacity = map->capacity * 2;
	map->array = realloc(map->array, map->capacity * sizeof(void*));
}

int hm_insert(struct hashmap *map, void *key, void *value) {
	if (map->size + 1 == map->capacity)
		hm_realloc(map);	
	if (!map->size) {

		struct hm_node *node = malloc(sizeof(struct hm_node));
		if (!node)
			return HM_CODE_ERROR;
		
		node->next = NULL;
		node->hash = map->hash(key);
		node->value = value;
		node->key = key;

		*(map->array) = node;
	} else {
		struct hm_node *current = *(map->array);
		while(current->next) {
			if (map->compare(current->key, key) == 0) {
				current->value = value;
				return HM_CODE_REPLACED;
			}
			current = current->next;
		}

		struct hm_node *node = malloc(sizeof(struct hm_node));
		if (!node)
			return HM_CODE_ERROR;
		
		node->next = NULL;
		node->hash = map->hash(key);
		node->value = value;
		node->key = key;

		current->next = node;
		map->array[map->size] = node;
	}

	++map->size;
	return HM_CODE_INSERTED;
}

int hm_replace(struct hashmap *map, void *key, void *value, void **old) {
	struct hm_node *node_get = hm_get_node(map, key);
	void *temp = NULL;
	
	if (node_get)
		temp = hm_get_node(map, key)->value;

	int insert_code = hm_insert(map, key, value);
	if(insert_code == HM_CODE_REPLACED)
		if(old != temp)
			old = temp;

	return insert_code;
}

void *hm_remove(struct hashmap *map, void *key) {
	struct hm_node *result = hm_get_node(map, key);
	if (result) {
		void *value = result->value;
		hm_node_destroy(map, result);
		return value;
	}

	return NULL;	
}

int hm_resize(struct hashmap *map, size_t new_capacity) {
	map->capacity = new_capacity;
	map->array = realloc(map->array, new_capacity);
	if (!map->array)
		return HM_CODE_ERROR;
	return HM_CODE_SUCCESS;
}

struct hm_iter *hm_iter_init(struct hashmap *map, int flags) {
	struct hm_iter *new_iter = malloc(sizeof(struct hm_iter));
	if (!new_iter)
		return NULL;

	new_iter->flags = flags;
	new_iter->pos = 0;
	new_iter->map = map;
	new_iter->next = map->array[0];

	return new_iter;
}

void hm_iter_destroy(struct hm_iter *iter) {
	free(iter); //C'est tout ? //FIXME
}

struct hm_node *hm_iter_next(struct hm_iter *iter) {
	if (!iter->next)
		return NULL;
	struct hm_node *node = iter->next;
	iter->next = iter->next->next;
	++iter->pos;

	return node;
}

void hm_size_change(struct hashmap *map, size_t size) {
	map->size = size;
}
