#include <stdlib.h>
#include <err.h>

#include "libstr.h"

struct shnode {
	struct shnode *next;
	long hash;
	char *value;
};

struct shmap {
	size_t capacity;
	size_t size;
	
	struct shnode **array;
};

/**
 * Initializes a simple hmap with a capacity of capacity. Must
 * be superior to 0.
 */
struct shmap *sh_init(size_t capacity);

/**
 * Frees the simple hmap passed as parameter. Do not use it afterwards.
 */
void sh_free(struct shmap *map);

/**
 * Inserts a new node in the hmap, with the value specified as parameter.
 * The key of that value is hash(value);
 */
void sh_insert(struct shmap *map, char *value);

/**
 * Returns a node based on its key.
 */
struct shnode *sh_get_node(struct shmap *map, long key); 

/**
 * Returns the value of a node based on its key.
 */
char *sh_get(struct shmap *map, long key); 


