/*
 * Test Suite for my Linked List implementation
 *
 * Execute test_suite() for the full test suite
 */
#include <stdio.h>
#include <err.h>

#include "color.h"

#include "libstr.h"
#include "linked_list.h"
#include "simple_vector.h"
#include "vector.h"
#include "tdd.h"
#include "hashmap.h"
#include "shmap.h"
#include "search_tree.h"

#define TEST_NB 256
#define MAX_TEST_NB 4096 * 10

#define VALUE_0 27
#define VALUE_1 729
#define VALUE_2 255
#define VALUE_3 128

#define BUFFER_SIZE 256

/*
 * Linked List tests
 */
void test_llist_is_empty();
void test_llist_push();
void test_llist_pop();
void test_llist_reverse();
void test_llist_clean();
void test_llist_reverse();
void test_llist_stress();
void test_llist_delete();
void test_llist_find();
void test_llist_is_in();

/*
 * Vector tests
 */
void test_vector_stress();
void test_vector_push();
void test_vector_pop();
void test_vector_reset();
void test_vector_sort_ab();
void test_vector_sort();
void test_vector_memleak();

/*
 * Hashmap tests
 */
void test_hashmap_create();
void test_hashmap_stat_funcs();
void test_hashmap_insert();
void test_hashmap_get_node();

/*
 * Executes all the test functions declared above
 */
void test_suite();
