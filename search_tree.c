#include "search_tree.h"

long hash(char *key) {
  size_t i = 0;
  uint32_t hash = 0;

  while (i != strlen(key)) {
    hash += key[i++];
    hash += hash << 10;
    hash ^= hash >> 6;
  }

  hash += hash << 3;
  hash ^= hash >> 11;
  hash += hash << 15;

  return hash;
}

search_tree *search_tree_init(char *link) {
	search_tree *new_tree = malloc(sizeof(search_tree));
	if (!new_tree)
		return NULL;

	new_tree->hash = hash(link);
	new_tree->link = link;
	new_tree->left = NULL;
	new_tree->right = NULL;
	
	return new_tree;
}

static search_tree *search_tree_init_hash(char *link, long hash) {
	search_tree *new_tree = malloc(sizeof(search_tree));
	if (!new_tree)
		return NULL;

	new_tree->hash = hash;
	new_tree->link = link;
	new_tree->left = NULL;
	new_tree->right = NULL;
	
	return new_tree;
}

void search_tree_destroy(search_tree *tree) {
	if (tree->left)
		search_tree_destroy(tree->left);
	if (tree->right)
		search_tree_destroy(tree->right);
	if (tree)
		free(tree);
}

int __search_tree_insert(search_tree *current, char *link, long hash) {
	if (current->hash == hash)
		return ST_REPLACED;
	// If the hash respects uniqueness, we don't need to actually do anything

	if (hash < current->hash) {
		if (!current->left) {
			search_tree *to_place = search_tree_init_hash(link, hash);
			if (!to_place)
				return ST_ERROR;

			current->left = to_place;
			return ST_INSERTED;
		}
		return __search_tree_insert(current->left, link, hash);
	} else {
		if (!current->right) {
			search_tree *to_place = search_tree_init_hash(link, hash);
			if (!to_place)
				return ST_ERROR;

			current->right = to_place;
			return ST_INSERTED;
		}
		return __search_tree_insert(current->right, link, hash);	
	}	

	return ST_ERROR;

}

int search_tree_insert(search_tree *tree, char *link) {
	long new_hash = hash(link);
	return (__search_tree_insert(tree, link, new_hash));
}
