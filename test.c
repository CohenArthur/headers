#include "test.h"

static int compare_int(void *left, void *right){
	if (*((int *) left) > *((int *) right))
		return 1;
	if (*((int *) left) < *((int *) right))
		return -1;
	return 0;
}

static long stupid_hash(void *ptr){
	long hash = 1;
	int *ptr_value = (int *) ptr;

	return hash << *ptr_value;
}

/*
 *
 * ===============================LIBRARY STR==================================
 *
 */
void test_char_lower_case() {
	char *alert = "test_char_lower_case()";

	assert_char(char_lower_case('a'), 'a', alert); 
	assert_char(char_lower_case('A'), 'a', alert);
	assert_char(char_lower_case('z'), 'z', alert);
	assert_char(char_lower_case('Z'), 'z', alert);

	success(alert);
}

void test_char_upper_case() {
	char *alert = "test_char_lower_case()";
	
	assert_char(char_upper_case('A'), 'A', alert);
	assert_char(char_upper_case('a'), 'A', alert);
	assert_char(char_upper_case('Z'), 'Z', alert);
	assert_char(char_upper_case('z'), 'Z', alert);

	success(alert);
}

void test_char_is_whitespace() {
	char *alert = "test_char_is_whitespace()";

	assert_int(char_is_whitespace('A'), 0, alert);
	assert_int(char_is_whitespace(' '), 1, alert); 
	assert_int(char_is_whitespace('\t'), 1, alert); 
	assert_int(char_is_whitespace('a'), 0, alert); 
	assert_int(char_is_whitespace('Z'), 0, alert); 

	success(alert);
}

void test_str_lower_case() {
	char *alert = "test_str_lower_case()";

	char str[] = "YEET";
	str_lower_case(str);
	assert_string(str, "yeet", alert); 
	
	char str1[] = "yeet";
	str_lower_case(str1);
	assert_string(str1, "yeet", alert);

	char str2[] = "Yeet";
	str_lower_case(str2);
	assert_string(str2, "yeet", alert);

	char str3[] = "yEET";
	str_lower_case(str3);
	assert_string(str3, "yeet", alert);

	success(alert);
}

void test_str_upper_case() {
	char *alert = "test_str_upper_case()";

	char str[] = "yeet";
	str_upper_case(str);
	assert_string(str, "YEET", alert);
	
	char str1[] = "YEET";
	str_upper_case(str1);
	assert_string(str1, "YEET", alert);

	success(alert);
}

void test_str_is_whitespace() {
	char *alert = "test_str_is_whitespace()";

	char str[] = "    \t";
	assert_int(str_is_whitespace(str), 1, alert);

	char str1[] = "yeet\n";
	assert_int(str_is_whitespace(str1), 0, alert);
	
	char str2[] = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
	assert_int(str_is_whitespace(str2), 1, alert);
	
	char str3[] = "    ";
	assert_int(str_is_whitespace(str3), 1, alert);

	success(alert);
}

void test_str_compare() {
	char *alert = "test_str_compare()";

	assert_int(str_compare("yeet", "yeet"), 0, alert);
	assert_int_inferior(str_compare("Yeet", "yeet"), 0, alert); 
	assert_int_superior(str_compare("yeet", "ye"), 0, alert);	
	assert_int_inferior(str_compare("yeet", "yeetus my jeezus"), 0, alert);	
	assert_int_superior(str_compare("yeetus my jeezus", "yeet"), 0, alert);	

	success(alert);
}

void test_str_compare_ignore_case() {
	char *alert = "test_str_compare_ignore_case()";

	assert_int(str_compare_ignore_case("yeet", "yeet"), 0, alert);
	assert_int(str_compare_ignore_case("yeet", "Yeet"), 0, alert);
	assert_int(str_compare_ignore_case("yeet", "yEet"), 0, alert);
//	assert_int(str_compare_ignore_case("yeet", "yeET"), 0, alert);
//	assert_int_superior(str_compare_ignore_case("yeetus my jeezus", "yeet"), 0, alert);	

	success(alert);
}

void test_str_hash() {
	char *alert = "test_str_hash";



	success(alert);
}

void test_str_hash_ignore_case() {
	char *alert = "test_str_hash_ignore_case";



	success(alert);
}

void test_str_dump_escape() {
	char *alert = "test_str_dump_escape()";



	success(alert);
}

void test_str_in() {
	char *alert = "test_str_in()";
/*
	assert_long(str_in("Coucou", "C"), 27, alert); //FIXME
	assert_long(str_in("Coucou", "o"), 1, alert);
	assert_long(str_in("Coucou", "z"), -1, alert);
	assert_long(str_in("Coucou", "cou"), 3, alert);
	assert_long(str_in("Coucou", "COU"), -1, alert);
*/
	success(alert);
}

/*
 *
 * ===============================LINKED LIST==================================
 *
 */
void test_llist_is_empty() {
	char *alert = "llist_is_empty()";
	LIST *list = llist_init();
	assert_int(llist_is_empty(list), 1, alert);

	llist_push(list, VALUE_0);
	assert_int(llist_is_empty(list), 0, alert);

	llist_destroy(list);
	success(alert);
}

void test_llist_push() {
	char *alert = "llist_push()";

	LIST *list = llist_init();
	llist_push(list, VALUE_0);
	llist_push(list, 729);
	assert_int(list->next->value, VALUE_0, alert);
	assert_int(list->next->next->value, 729, alert);

	llist_destroy(list);
	success(alert);
}

void test_llist_pop() {
	char *alert = "llist_pop()";

	LIST *list = llist_init();
	llist_push(list, VALUE_0);
	assert_int(llist_pop(list), VALUE_0, alert);

	llist_push(list, 729);
	assert_int(llist_pop(list), 729, alert);

	for (size_t index = 0; index < MAX_TEST_NB; index++) {
		llist_push(list, index * index);
		assert_int(llist_pop(list), index * index, alert);
		progress(index, alert);
	}

	llist_destroy(list);
	success(alert);
}

void test_llist_clean() {
	char *alert = "llist_clean()";
	LIST *list = llist_init();

	for (size_t index = 0; index < TEST_NB; index++) {
		llist_push(list, index * index);
		progress(index, alert);
	}

	llist_clean(list);
	assert_int(list->next->value, 0, alert);

	llist_destroy(list);
	success(alert);
}

void test_llist_reverse() {
	char *alert = "llist_reverse()";
	LIST *list = llist_init();
	llist_push(list, VALUE_0);
	llist_push(list, 729);
	llist_reverse(list);

	assert_int(list->next->value, 729, alert);//Fail first 729
	assert_int(list->next->next->value, VALUE_0, alert);

	llist_destroy(list);
	success(alert);
}

void test_llist_stress() {
	char *alert = "llist_stress()";

	for (size_t index = 0; index < MAX_TEST_NB; index++) {
		LIST *list = llist_init();
		llist_push(list, index);
		llist_destroy(list);
		progress(index, alert);
	}

	success(alert);
}

void test_llist_delete() {
	char *alert = "llist_delete()";

	LIST *list = llist_init();
	llist_push(list, VALUE_0);
	llist_push(list, 729);

	llist_delete(list, VALUE_0);

	assert_int(list->next->value, 729, alert);

	llist_destroy(list);
	success(alert);
}

void test_llist_find() {
	char *alert = "llist_find()";
	LIST *list = llist_init();

	llist_push(list, VALUE_0);
	llist_push(list, 729);

	LIST *find_27;
	LIST *find_729;

	find_27 = llist_find(list, VALUE_0);
	assert_int(find_27->value, VALUE_0, alert);

	find_729 = llist_find(list, 729);
	assert_int(find_729->value, 729, alert);

	llist_destroy(list);
	success(alert);
}

void test_llist_is_in() {
	char *alert = "llist_is_in()";
	LIST *list = llist_init();

	llist_push(list, VALUE_0);
	llist_push(list, 729);

	assert_int(llist_is_in(list, VALUE_0), 1, alert);
	assert_int(llist_is_in(list, 729), 1, alert); //FIXME
	assert_int(llist_is_in(list, 404), 0, alert);

	llist_destroy(list);
	success(alert);
}

/*
 *
 * =============================SIMPLE VECTORS=================================
 *
 */
void test_svector_memleak() {
	char *alert = "test_svector_memleak()";

	struct simple_vector *vect = v_init(VALUE_1);
	v_free(vect);	

	success(alert);
}

void test_svector_insert() {
	char *alert = "test_svector_insert()";

	struct simple_vector *vect = v_init(VALUE_0);
	assert_int(vect->size, 0, alert);
	assert_int(vect->capacity, VALUE_0, alert);

	v_add(vect, VALUE_1);
	assert_int(vect->array[1], VALUE_1, alert);
	assert_int(v_pop(vect), VALUE_1, alert);

	v_free(vect);
	success(alert);
}

void test_svector_pop() {
	char *alert = "test_svector_pop()";

	struct simple_vector *vect = v_init(1);
	v_add(vect, VALUE_0);
	v_add(vect, VALUE_1);
	v_add(vect, VALUE_2);
	v_add(vect, VALUE_3);

	assert_int(v_pop(vect), VALUE_3, alert);
	assert_int(v_pop(vect), VALUE_2, alert);
	assert_int(v_pop(vect), VALUE_1, alert);
	assert_int(v_pop(vect), VALUE_0, alert);

	v_free(vect);
	success(alert);
}

void test_svector_at() {
	char *alert = "test_svector_at()";

	struct simple_vector *vect = v_init(1);

	v_add(vect, VALUE_0);
	v_add(vect, VALUE_1);
	v_add(vect, VALUE_2);
	v_add(vect, VALUE_3);

	assert_int(v_at(vect, 3), VALUE_3, alert);
	assert_int(v_at(vect, 2), VALUE_2, alert);
	assert_int(v_at(vect, 1), VALUE_1, alert);
	assert_int(v_at(vect, 0), VALUE_0, alert);

	v_free(vect);
	success(alert);
}

void test_svector_find() {
	char *alert = "test_svector_find()";

	struct simple_vector *vect = v_init(1);

	v_add(vect, VALUE_0);
	v_add(vect, VALUE_1);
	v_add(vect, VALUE_2);
	v_add(vect, VALUE_3);

	assert_int(v_find(vect, VALUE_3), 3, alert);
	assert_int(v_find(vect, VALUE_2), 2, alert);
	assert_int(v_find(vect, VALUE_1), 1, alert);
	assert_int(v_find(vect, VALUE_0), 0, alert);

	v_free(vect);
	success(alert);
}

/*
 *
 * =================================VECTORS====================================
 *
 */
void test_vector_stress() {
	char *alert = "vector stress";

	for (size_t index = 0; index < MAX_TEST_NB; index++) {
		VECTOR *vect = vector_init(index + 1, NULL);

		vector_free(vect);
		progress(index, alert);
	}

	success(alert);
}

void test_vector_insert_pop() {
	char *alert = "vector_insert_pop()";
	VECTOR *vect = vector_init(1, NULL);

	int push_1 = VALUE_0;
	int push_2 = VALUE_1;

	vector_push(vect, &push_1);
	vector_push(vect, &push_2);

	int value_729 = *((int *)vector_pop(vect));
	int value_27 = *((int *)vector_pop(vect));

	assert_int(value_729, VALUE_1, alert);
	assert_int(value_27, VALUE_0, alert);

	vector_free(vect);
	success(alert);
}

void test_vector_memleak() {
	char *alert = "vector memory leaks";
	VECTOR *vect = vector_init(1, NULL);

	for(size_t index = 0; index < MAX_TEST_NB; index++)
		for(size_t _sec_index = 0; _sec_index < 10; _sec_index++)
			vector_push(vect, &index);

	vector_free(vect);
	success(alert);
}

void test_vector_sort() { 
	char *alert = "vector_sort()";
	VECTOR *vect = vector_init(1, NULL);
	vect->compare = compare_int;
	
	int push_0 = VALUE_0;
	int push_1 = VALUE_1;
	int push_2 = VALUE_2;
	int push_3 = VALUE_3;

	vector_push(vect, &push_0);
	vector_push(vect, &push_1);
	vector_push(vect, &push_2);
	vector_push(vect, &push_3);

	assert_int(compare_int(vector_at(vect, 1), &push_0), 0, alert);
	assert_int(compare_int(vector_at(vect, 2), &push_1), 0, alert);
	assert_int(compare_int(vector_at(vect, 3), &push_2), 0, alert);
	assert_int(compare_int(vector_at(vect, 3), &push_3), 0, alert);

//	vector_sort(vect); //FIXME Segfault here

	vector_free(vect);
	success(alert);
}

/*
 *
 * ================================HASHMAPS====================================
 *
 */
void test_hashmap_create(){
	char *alert = "test_hashmap_create()";
	
	struct hashmap *hash = hm_create(VALUE_0, NULL, NULL,
		HM_MAP_AUTO_RESIZE|HM_MAP_FREE_KEY);

	assert_long(hash->capacity, VALUE_0, alert);
	assert_int(hash->flags, HM_MAP_AUTO_RESIZE|HM_MAP_FREE_KEY, alert);

	struct hashmap *hash_str = hm_create_str(VALUE_0, 0);
	assert_long(hash_str->capacity, VALUE_0, alert);

	hm_resize(hash, hash->capacity * 3);
	hm_resize(hash, hash->capacity * 15);

	hm_destroy(hash);
	hm_destroy(hash_str);
	success(alert);
}

void test_hashmap_stat_funcs(){
	char *alert = "test_hashmap_stat_funcs()";
	struct hashmap *map = hm_create(VALUE_0, stupid_hash, compare_int, 0);

	int zero = 0;
	int two = 2;
	int left = VALUE_0;
	int right = VALUE_0;

	assert_int(map->compare(&left, &right), 0, alert);

	right = VALUE_1;

	assert_int(map->compare(&right, &left), 1, alert);
	assert_int(map->compare(&left, &right), -1, alert);

	assert_int(map->hash(&zero), 1, alert);
	assert_int(map->hash(&two), 4, alert);

	hm_destroy(map);
	success(alert);	
}

void test_hashmap_insert(){
	char *alert = "test_hashmap_insert()";
	
	struct hashmap *map = hm_create(VALUE_3, stupid_hash, compare_int, 0);

	int key = VALUE_0;
	int value = VALUE_1;	
	hm_insert(map, &key, &value);

	key = value;
	value = VALUE_2;
	hm_insert(map, &key, &value);
	hm_insert(map, &key, &key);
	hm_insert(map, &value, &value);
	hm_insert(map, &value, &key);

	for(size_t index = 0; index < 300; ++index) {
		int x = index + 1;
		hm_insert(map, &x, &value);
	}
	for(size_t index = 0; index < 300; ++index) {
		int x = index + 1;
		hm_insert(map, &x, &value);
	}
	for(size_t index = 0; index < 300; ++index) {
		int x = index + 1;
		hm_insert(map, &x, &value);
	}
	for(size_t index = 0; index < 300; ++index) {
		int x = index + 1;
		hm_insert(map, &x, &value);
	}

	hm_destroy(map);
	success(alert);
}

void test_hashmap_replace(){
	char *alert = "test_hashmap_replace()";
	
	struct hashmap *map = hm_create(VALUE_3, stupid_hash, compare_int, 0);

	int key = VALUE_0;
	int value = VALUE_1;	
	hm_insert(map, &key, &value);

	void **old = malloc(sizeof(void *));

	key = value;
	value = VALUE_2;
	hm_replace(map, &key, &value, old);
	hm_replace(map, &key, &key, old);
	hm_replace(map, &value, &value, old);
	hm_replace(map, &value, &key, old);

	free(old);
	hm_destroy(map);
	success(alert);
}

void test_hashmap_get_node(){
	char *alert = "test_hashmap_get_node()";

	struct hashmap *map = hm_create(VALUE_0, stupid_hash, compare_int, 0);
	
	int value = VALUE_0;
	int key = VALUE_0;
	hm_insert(map, &key, &value);

	struct hm_node *node = hm_get_node(map, &key);
	assert_long(node->hash, map->hash(&key), alert);

	hm_destroy(map);
	success(alert);
}

void test_hashmap_get(){
	char *alert = "test_hashmap_get()";

	struct hashmap *map = hm_create(VALUE_0, stupid_hash, compare_int, 0);	
	int value = VALUE_0;
	int key = VALUE_0;
	hm_insert(map, &key, &value);

	int get_value = *((int *) hm_get(map, &key));
	assert_int(get_value, value, alert);

	hm_destroy(map);
	success(alert);
}

void test_hashmap_iter() {
	char *alert = "test_hashmap_iter()";

	struct hashmap *map = hm_create(VALUE_0, stupid_hash, compare_int, 0);	

	int value = VALUE_0;
	int key = VALUE_0;
	hm_insert(map, &key, &value);
	hm_insert(map, &value, &key);

	struct hm_iter *iter = hm_iter_init(map, 0); 
	struct hm_node *node;

	while((node = hm_iter_next(iter)))
		assert_long(node->hash, map->hash(node->key), alert);

	hm_iter_destroy(iter);
	hm_destroy(map);
	success(alert);
}

void test_shmap_init() {
	char *alert = "test_shmap_init()";

	struct shmap *map = sh_init(VALUE_1);

	assert_long(map->capacity, VALUE_1, alert);
	assert_long(map->size, 0, alert);

	sh_free(map);
	success(alert);
}

void test_shmap_insert() {
	char *alert = "test_shmap_insert()";

	struct shmap *map = sh_init(1);

	sh_insert(map, alert);
	long alert_hash = str_hash(alert);

	assert_long(map->size, 1, alert);
	assert_long(map->array[0]->hash, alert_hash, alert);
	assert_int(str_compare(map->array[0]->value, alert), 0, alert);

	sh_insert(map, alert);

	assert_long(map->size, 2, alert);
	assert_long(map->array[1]->hash, alert_hash, alert);
	assert_int(str_compare(map->array[1]->value, alert), 0, alert);
	
	sh_free(map);
	success(alert);
}

void test_shmap_get() {
	char *alert = "test_shmap_get()";

	struct shmap *map = sh_init(VALUE_1);

	char *str_1 = "Better call Saul";
	char *str_2 = "Heisenberg";
	char *str_3 = "Jesse Pinkman";

	sh_insert(map, alert);
	sh_insert(map, str_1);
	sh_insert(map, str_2);
	sh_insert(map, str_3);

	assert_string(sh_get(map, str_hash(str_1)), str_1, alert);
	assert_string(sh_get(map, str_hash(str_2)), str_2, alert);
	assert_string(sh_get(map, str_hash(alert)), alert, alert);
	assert_string(sh_get(map, str_hash(str_3)), str_3, alert);
	
	sh_free(map);
	success(alert);
}

/*
 *
 * ===============================SEARCH TREE==================================
 *
 */
void test_search_tree_memleak() {
	char *alert = "test_search_tree_memleak()";

	search_tree *tree = search_tree_init("M4A1");

	search_tree *first_left = search_tree_init("Yeet");
	search_tree *first_right = search_tree_init("Yote");	
	search_tree *second_left = search_tree_init("Yatzeeh"); 
	
	first_left->left = second_left;
	tree->left = first_left;
	tree->right = first_right;

	search_tree_destroy(tree);

	success(alert);
}

void test_search_tree_insert_pop() {
	char *alert = "test_search_tree_insert_pop()";

	search_tree *tree = search_tree_init("Weapon_mods");

	search_tree_insert(tree, "Weapon_mods");
	search_tree_insert(tree, "M4A1");
	search_tree_insert(tree, "AK74");

	assert_int(search_tree_insert(tree, "M4A1"), ST_REPLACED, alert); //FIXME
	assert_int(search_tree_insert(tree, "MP71"), ST_INSERTED, alert);
	assert_int(search_tree_insert(tree, "MP71"), ST_REPLACED, alert);

	search_tree_insert(tree, "ZZP72");

//	search_tree_print(tree); //FIXME

	assert_long(tree->left->hash, hash("M4A1"), alert);
	assert_long(tree->right->hash, hash("AK74"), alert);
	assert_long(tree->left->right->hash, hash("MP71"), alert);
	assert_long(tree->right->right->hash, hash("ZZP72"), alert);

	search_tree_destroy(tree);
	success(alert);
}

/*
void test_vector_sort_ab();
void test_vector_sort();
*/

void test_suite_search_tree() {
	test_search_tree_memleak();
	test_search_tree_insert_pop();
}

void test_suite_hmap() {
	test_hashmap_create();
	test_hashmap_insert();
	test_hashmap_get_node();
	test_hashmap_stat_funcs();
	test_hashmap_get();
	test_hashmap_replace();
	test_hashmap_iter();
}

void test_suite_svector() {
	test_svector_memleak();
	test_svector_insert();
	test_svector_pop();
	test_svector_at();
	test_svector_find();
}

void test_suite_vector() {
	test_vector_stress();
	test_vector_insert_pop();
	test_vector_memleak();
//	test_vector_sort(); //FIXME
}

void test_suite_libstr() {
	test_char_lower_case();
	test_char_upper_case();
	test_char_is_whitespace();
	test_str_lower_case();
	test_str_upper_case();
	test_str_is_whitespace();
	test_str_compare();
//	test_str_compare_ignore_case(); //FIXME
	test_str_in();
}

void test_suite_llist() {
	test_llist_is_empty();
	test_llist_push();
	test_llist_pop();
	test_llist_clean();
	test_llist_stress();
	test_llist_delete();
	test_llist_find();
	test_llist_is_in();
	//test_llist_reverse(); //FIXME
}

void test_suite_shmap() {
	test_shmap_init();
	test_shmap_insert();
	test_shmap_get();
}

void test_suite() {
	test_suite_llist();
	test_suite_libstr();
	test_suite_svector();
	test_suite_vector();
	test_suite_shmap();
	test_suite_hmap();
	test_suite_search_tree();

	printf("\nAll tests passed !\n");
}
