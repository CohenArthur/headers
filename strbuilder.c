#include "strbuilder.h"

struct strbuilder *create_strbuilder(void) {
    struct strbuilder *new_str = malloc(sizeof(new_str));
    if (!new_str)
        errx(1, "Creating a new String Builder failed.");
    return new_str;
}

void clear_strbuilder(struct strbuilder *builder);

void append_strbuilder(struct strbuilder *builder, char *str);

void add_strbuilder(struct strbuilder *builder, char chr);

int addint_strbuilder(struct strbuilder *builder, long value, int base);

char *tostr_strbuilder(struct strbuilder *builder);

void free_strbuilder(struct strbuilder *builder);

char *str_cpy(char *src);

char *itoa(int val, int base);
