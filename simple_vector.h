#include <stdlib.h>
#include <err.h>
#include <assert.h>

struct simple_vector {
	int 	*array;
	size_t 	size;
	size_t 	capacity;
};

/**
 * Allocates a vector and returns it
 */
struct simple_vector *v_init(size_t capacity);

/**
 * Frees the allocated memory of the vector.
 * Don't use it afterwards
 */
void v_free(struct simple_vector *vector);

/**
 * Inserts a value at the end of the vector
 */
void v_add(struct simple_vector *vector, int value);

/**
 * Deletes the value at the end of the vector and returns it
 */
int v_pop(struct simple_vector *vector);

/**
 * Returns the value at the desired position, without deleting it
 */
int v_at(struct simple_vector *vector, size_t index);

/**
 * Returns the index of the desired number, -1 if it isn't present in the vector
 */
size_t v_find(struct simple_vector *vector, int value);
