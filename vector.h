#pragma once

#include <stddef.h>

#define VECTOR struct vector //NEW

typedef int (*cmp_fnct)(void *, void *);

struct vector
{
    void **data;
    size_t index;       //Where we at
    size_t capacity;    //Max capacity

    void (*free_fnct)(void*);
    cmp_fnct compare;
};

/**
 * Allocate and returns a vector of the specified capacity.
 *
 * if 'free_fnct' is not NULL, then the function will be applied on elements
 * to destroy.
 *
 * Returns NULL if allocations failed.
 */
struct vector *vector_init(size_t capacity, void (*free_fnct)(void*));

/**
 * Free the vector 'v' and applied the free_fnct, if not NULL, on each items
 */
void vector_free(struct vector *v);

/**
 * Insert the 'item' in the vector 'v'
 * If insertion failed, returns 0
 * Else, returns 1
 */
int vector_push(struct vector *v, void *item); //index++;

/**
 * Pop the last pushed item in the vector 'v'
 * If error occures, returns NULL
 * Else, returns the elem
 */
void *vector_pop(struct vector *v);

/**
 * Delete all elements of the vector.
 * If 'free_fnct' != NULL, the function will be applied on all elems
 */
void vector_reset(struct vector *v);


/**
 * Inplace sort of the vector. Sort vector from min to max included.
 *
 * @param vec The vector to sort
 * @param min The lower bound
 * @param max The upper bound
 */
void vector_sort_ab(struct vector *vec, int min, int max);

/**
 * Same behaviour than vector_sort_ab(vec, 0, vector_size(vec) - 1)
 *
 * @param v The vector to sort
 */
void vector_sort(struct vector *v);

/**
 * Returns the elem at the specified 'index'.
 * If 'index' is out of bounds, returns NULL
 */
static inline void *vector_at(const struct vector *v, size_t index)
{
    return index < v->index ? v->data[index] : NULL;
}

/**
 * Returns the size of the vector
 */
static inline size_t vector_size(const struct vector *v)
{
    return v->index;
}
