#include "simple_vector.h"

struct simple_vector *v_init(size_t capacity) {
	if (!capacity)
		errx(1, "Cannot init a vector with capacity of 0");
	
	int *new_array = malloc(sizeof(int) * capacity);
	struct simple_vector *new_v = malloc(sizeof(struct simple_vector));

	new_v->size = 0;
	new_v->capacity = capacity;
	new_v->array = new_array;

	return new_v;	
}

void v_free(struct simple_vector *vector) {
	assert(vector);
	free(vector->array);
	free(vector);
}

static void v_realloc(struct simple_vector *vector) {
	assert(vector);

	vector->capacity = vector->capacity * 2;
	
	vector->array = realloc(vector->array, sizeof(int) * vector->capacity);
	if (!vector->array)
		errx(1, "gmfm f,urur,t,mdfy,d,umetkmnstuttsmsmfuck");
}

void v_add(struct simple_vector *vector, int value) {
	assert(vector);

	++vector->size;
	if (vector->size == vector->capacity)
		v_realloc(vector);
	
	vector->array[vector->size] = value;
}

int v_pop(struct simple_vector *vector) {
	assert(vector);

	int value = vector->array[vector->size];
	--vector->size;
	return value;
}

int v_at(struct simple_vector *vector, size_t index) {
	assert(vector);

	++index;

	if (index > vector->size)
		errx(1, "Trying to access value outside of bounds");

	return vector->array[index];
}

size_t v_find(struct simple_vector *vector, int value) {
	assert(vector);

	for(size_t index = 0; index <= vector->size; ++index)
		if (vector->array[index] == value)
			return index - 1;

	return -1;
}
