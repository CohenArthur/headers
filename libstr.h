# pragma once

#include <stddef.h>
#include <stdio.h>

/**
 * If c is in [A-Z], return is correspoding value in [a-z]
 */
char char_lower_case(char c);

/**
 * If c is in [a-z], return is corresponding value in [A-Z]
 */
char char_upper_case(char c);

/**
 * Return 1 if s is a blank character (space, tab, ...)
 */
int char_is_whitespace(char c);

/**
 * Map all characters of [A-Z] to [a-z]
 */
void str_lower_case(char *s);

/**
 * Map all characters of [a-z] to [A-Z]
 */
void str_upper_case(char *s);

/**
 * Return 1 if s is a blank string (only space, tabs, ...).
 * Otherwise, return 0.
 */
int str_is_whitespace(const char *s);

/**
 * Same behaviour than strcmp(3)
 */
int str_compare(const char *s1, const char *s2);

/**
 * Same behaviour than strcmp(3), ignoring the case
 */
int str_compare_ignore_case(const char *s1, const char *s2);

/**
 * Hash the string 's'
 * hash(s1) = hash(s2) => compare(s1, s2) = 0
 */
long str_hash(const char *s);

/**
 * Hash the string 's'.
 * hash(s1) = hash(s2) => compare_ignore_case(s1, s2) = 0
 */
long str_hash_ignore_case(const char *s);

/**
 * Print a string in a file, escaping ('\') a character
 */
void str_dump_escape(FILE *f, const char *s, char escape);

/**
 * Implements the KMP algorithm.
 * Returns the starting position of the substring in the string if it exists,
 * -1 if not.
 */
long str_in(char *string, char *substring);
