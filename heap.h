#pragma once

#include <stddef.h>
#include <stdlib.h>

/**
 * Simple comparison function typedef :
 *
 * Returns a negative value when l_data < r_data,
 * return 0 if l_data == r_data,
 * return a positive value if l_data > r_data
 */
typedef int (*heap_cmp_function_t)(void *l_data, void *r_data);

/**
 * Simple heap struct
 *
 * @data : Pointer to the data stored
 * @data_cmp_function : Comparison function between two instances of data
 * @left : Left child of the current node. NULL when a leaf
 * @right : Right child of the current node. NULL when a leaf
 */
struct heap
{
    void *data;

    heap_cmp_function_t data_cmp_function;

    struct heap *left;
    struct heap *right;
};

/**
 * Initializes a heap and returns it. Returns NULL if a problem occurs
 */
struct heap *heap_init(void *data, heap_cmp_function_t cmp_function);

/**
 * Destroy the heap passed as argument
 */
void heap_free(struct heap *heap);

