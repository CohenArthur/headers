# Headers

Simple repository containing different headers and source files useful for many different C projects

Table of Contents :

- `exec_cmd.h` executes a program with a set of arguments, returns the different statuses in a `struct status`
- `newline_counter.h` returns the number of lines (\n) in a file
- `shmap.h` extremely simple hashmap. It contains the following functions :
    - `sh_init()` Initializes the hashmap
    - `sh_free()` Frees the hashmap
    - `sh_insert()` Inserts a new node at the end of the hashmap
    - `sh_get_node()` Returns a node based on its key
- `simple_vector.h` is an extremely simple vectors. It contains the following functions :
    - `v_init()` Initializes a vector
    - `v_free()` Destroys the vector
    - `v_add()` Adds a value at the end of the vector
    - `v_at()` Returns the value at the specified index
    - `v_pop()` Returns the last value and removes it from the vector
    - `v_find()` Returns the index of the first occurence of the specified value
- `linked_list.h` Basic linked list. Everything is working but `llist_reverse()`
- `hashmap.h` Complete hashmap built for any type.
- `vector.h` Complete vector type built for any type.
- `heap.h` Unimplemented
- `libstr.h` Simple rewriting of `string.h`, containing a few functions. Everything is working but `str_compare_ignore_case()`
- `strbuilder.h` Unimplemented
- `tdd.h` Simple "lib" for unit testing. Contains the following functions :
    - `assert_X()` to test the equivalence between two values of type X
    - `success()` to print the success of a test
    - `progress()` to show the progress of multiple unit tests (in a loop for example)
- `test.h` and `test.c` test suite for the project
