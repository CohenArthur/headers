# include "newline_counter.h"

#define BUFFER_SIZE 4096

size_t newline_counter(int fd) {
	size_t result = 0;
	char my_buff[4096];
	ssize_t read_value = 1;

	while(read_value != 0) {
		read_value = read(fd, &my_buff, BUFFER_SIZE);
		if (read_value == -1) {
			if (errno == EINTR)
				continue;
			err(3, "Reading failed.");
		}
		for(ssize_t index = 0; index < read_value; index++) {
			if (*(my_buff + index) == '\n')
				++result;
		}
	}

	return result;
}
