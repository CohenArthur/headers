# include <err.h>
# include <errno.h>
# include <sys/wait.h>
# include <stdlib.h>
# include <unistd.h>

/*
 * Structure containing the status of a program
 * - exited	  : 1 if the cmd exited correctly, 0 if not
 * - exit_status : Exit status of the cmd
 * - kill_signal : Signal sent to kill the cmd
 */
struct status {
  int exited, exit_status, kill_signal;
};

/*
 * Executes a program/shell command and stores the status in a struct status
 * - cmd	 : Command to execute : cat, ls, echo...
 * - args    : Args to pass to the command
 * - status  : Struct status to fill
 */
void exec_cmd(char *cmd, char **args, struct status *status);
