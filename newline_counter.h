# include <err.h>
# include <errno.h>
# include <stdlib.h>
# include <unistd.h>

/*
 * Returns the number of lines (\n) contained in a file
 * - fd : File Descriptor of the file you want to know the lines of
 */
size_t newline_counter(int fd);
