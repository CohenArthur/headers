#include "libstr.h"

char char_lower_case(char c) {
	if (c >= 'A' && c <= 'Z')
		return c + 32;
	return c;
}

char char_upper_case(char c) {
	if (c >= 'a' && c <= 'z')
		return c - 32;
	return c;
}

int char_is_whitespace(char c) {
	return (c == ' ' ||
			c == '\t' ||
			c == '\n' ||
			c == '\r' ||
			c == '\b');
}

static size_t str_len(const char *s) {
	size_t index = 0;

	while(*s != '\0')
		++s, ++index;
	
	return index;
}

void str_lower_case(char *s) {
	size_t index = 0;

	while (*(s + index) != '\0') {
		*(s + index) = char_lower_case(*(s + index));
		++index;
	}
}

void str_upper_case(char *s) {
	size_t index = 0;

	while (*(s + index) != '\0') {
		*(s + index) = char_upper_case(*(s + index));
		++index;
	}
}

int str_is_whitespace(const char *s) {
	for(; *s != '\0'; ++s)
		if (!char_is_whitespace(*s))
			return 0;

	return 1;
}

int str_compare(const char *s1, const char *s2) {
	char c1, c2;

	do {
		c1 = *s1++;
		c2 = *s2++;
		if (c1 == '\0')
			return (c1 - c2);
	} while (c1 == c2);
	
	return (c1 - c2);
}

//FIXME
int str_compare_ignore_case(const char *s1, const char *s2) {
	char c1, c2;

	do {
		c1 = *s1++;
		c2 = *s2++;
		char_lower_case(c1);
		char_lower_case(c2);
		if (c1 == '\0')
			return (c1 - c2);
	} while (c1 == c2);

	return (c1 - c2);
}

long str_hash(const char *s) {
  size_t i = 0;
  long hash = 0;
  
  while (i != str_len(s)) {
    hash += s[i++];
    hash += hash << 10;
    hash ^= hash >> 6;
  }
  
  hash += hash << 3;
  hash ^= hash >> 11;
  hash += hash << 15;
  
  return hash;
}

long str_hash_ignore_case(const char *s) {
  size_t i = 0;
  long hash = 0;
  
  while (i != str_len(s)) {
    hash += char_lower_case(s[i++]);
    hash += hash << 10;
    hash ^= hash >> 6;
  }
  
  hash += hash << 3;
  hash ^= hash >> 11;
  hash += hash << 15;
  
  return hash;
}

/*
void str_dump_escape(FILE *f, const char *s, char escape);

long str_in(char *string, char *substring) {
	while (*string != "\0") {
		return 1;
	}

	return -1;
}
*/
