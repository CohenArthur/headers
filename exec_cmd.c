# include "exec_cmd.h"

void exec_cmd(char *cmd, char **args, struct status *status) {
	pid_t pid = fork();
	int wstatus = 0;

	if (pid == -1)
		err(3, "Couldn't fork properly.");
	if (pid != 0) {
		waitpid(pid, &wstatus, 0);
		status->exited = WIFEXITED(wstatus);
		status->exit_status = WEXITSTATUS(wstatus);
		status->kill_signal = WTERMSIG(wstatus);
	} else {
		if (execvp(cmd, args) == -1) //FIXME
			err(3, "Execution of %s failed ", cmd);
	}
}

