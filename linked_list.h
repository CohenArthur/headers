#include <stdlib.h>
#include <stdio.h>
#include <err.h>

#define LIST  linked_list
#define LLIST linked_list

/*
 * Simple linked list structure : Can be tweaked to accept longs, strings...
 */
typedef struct linked_list{
	int    value;
	struct linked_list *next;
	//struct linked_list *last;
 	//struct linked_list *first;
} linked_list;

/*
 * Prints the linked list passed as argument
 */
void llist_print(LIST *list);

/*
 * Allocates and returns a pointer to a linked list
 */
LIST *llist_init(void);

/*
 * Frees the linked list passed as arg
 */
void llist_destroy(LIST *list);

/*
 * Checks if the list is empty
 */
int llist_is_empty(LIST *list);

/*
 * Closes the program and free the resources if the list is empty
 */
void llist_break_empty(LIST *list);

/*
 * Cleans the linked list, but keeps it allocated for further use
 */
void llist_clean(LIST *list);

/*
 * Adds a new element at the end of the linked list
 */
void llist_push(LIST *list, int value);

/*
 * Returns the last element of the linked list and removes it from the list
 */
int llist_pop(LIST *list);

/*
 * Reverses the linked list
 */
void llist_reverse(LIST *list);

/*
 * Deletes the first occurence of 'value' inside the linked list
 */
void llist_delete(LIST *list, int value);

/*
 * Finds the first occurence of 'value' in the linked list and returns it
 */
LIST *llist_find(LIST *list, int value);

/*
 * Returns a boolean regarding the presence of value in the linked list
 */
int llist_is_in(LIST *list, int value);
