#include "linked_list.h"

void llist_print(LIST *list) {
	int nb_of_elts = 0;
	if (!list->next)
		printf("Empty List\n");
	else 
		list = list->next; //Skip the sentinel
	while (list) {
		printf("# %d\nValue of Elt : %d\n",\
				nb_of_elts, list->value);
		++nb_of_elts;
		list = list->next;
	}
}

LIST *llist_init(void) {
	LIST *new_ll = malloc(sizeof(LIST));
	if (!new_ll)
		errx(1, "Malloc failed when initialising linked list");
	new_ll->next = NULL;
	return new_ll;
}

void llist_destroy(LIST *list) { 
	if (!list->next) //If list->next == NULL
		free(list);  //If it's the last element, frees it.
	else {
		llist_destroy(list->next);
		free(list);
	}
}

int llist_is_empty(LIST *list) {
	return (list->next == NULL);
}

void llist_break_empty(LIST *list) {
	if (llist_is_empty(list)) {
		free(list);
		errx(1, "List is empty !\n");
	}
}

void llist_clean(LIST *list) {
	llist_break_empty(list);
	while(list) {
		list->value = 0;
		list = list->next;
	}
}

void llist_push(LIST *list, int value) {
	while (list->next) { 
		list = list->next; 
	}
	LIST *new = llist_init();
	new->value = value;
	list->next = new;
}

int llist_pop(LIST *list) {
	llist_break_empty(list);

	LIST *last;
	int result = 0;

	while (list->next) {
		last = list;
		list = list->next;
	}

	result = list->value;
	last->next = NULL;
	free(list);

	return result;
}

void llist_reverse(LIST *list) { //FIXME
	llist_break_empty(list);
	
	//Store the sentinel and the first element in their own variables
	LIST *sentinel = list; 
	LIST *first = list->next;

	while (list) {
		if (!list->next)
			sentinel->next = list;
		else
			if (&list == &first)
				list->next = NULL;
			else 
				list->next = list->next->next;

		list = list->next;
	}
}

void llist_delete(LIST *list, int value) {
	llist_break_empty(list);

	LIST *previous = NULL; 

	while (list && list->value != value) {
		previous = list;
		if (!previous->next)
			errx(1, "The value %d is not in the list", value);
		list = list->next;
	}
	
	previous->next = list->next;
	free(list);
}

//FIXME
LIST *llist_find(LIST *list, int value) {
	llist_break_empty(list);

	while (list && list->value != value) {
		list = list->next;
	}

	return list;
}

int llist_is_in(LIST *list, int value) {
	llist_break_empty(list);
		
	while (list) {
		if (list->value == value)
			return 1;
		list = list->next;
	}

	return 0;
}
