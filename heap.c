#include "heap.h"

struct heap *heap_init(void *data, heap_cmp_function_t cmp_function)
{
    struct heap *new_heap = malloc(sizeof(struct heap));
    if (!new_heap)
        return NULL;

    new_heap->data_cmp_function = cmp_function;
    new_heap->data = data;

    new_heap->left = NULL;
    new_heap->right = NULL;
    new_heap->data = NULL;

    return new_heap;
}

void heap_free(struct heap *heap)
{
    if (!heap)
        return;

    if (heap->left)
        heap_free(heap->left);

    if (heap->right)
        heap_free(heap->right);

    free(heap);
}

